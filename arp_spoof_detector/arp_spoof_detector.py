#!/usr/bin/env python

# example usage: "sudo python3 arp_spoof_detector.py"

import scapy.all as scapy

def get_mac_for_ip(ip):
    # create an ARP object and set destination IP
    arp_request = scapy.ARP(pdst=ip)
    # create an ethernet frame with the broadcast MAC address
    broadcast = scapy.Ether(dst="ff:ff:ff:ff:ff:ff")
    # combine both objects
    arp_request_broadcast = broadcast / arp_request
    # send an ARP request to get the MAC of the IP
    answered_packets = scapy.srp(arp_request_broadcast, timeout=1, verbose=False)[0]

    return answered_packets[0][1].hwsrc

def sniff(interface):
    print(f"[+] Start looking for ARP spoofing on interface: {interface}")

    # prn specifies a callback function for everytime a packet gets captured.
    # We can use a filter to not obtain everything that goes over the network (mainly gibberish) 
    # -> e.g. filter by protocol (`filter="udp"`), filter by protocol/port (`filter="port 21"`), etc.
    scapy.sniff(iface=interface, store=False, prn=process_sniffed_packet)

def process_sniffed_packet(packet):
    # check if the packet is an ARP response ("is-at")
    if packet.haslayer(scapy.ARP) and packet[scapy.ARP].op == 2:
        try: 
            # validate that the MAC address in this ARP response actually corresponds to the provided IP address
            real_mac = get_mac_for_ip(packet[scapy.ARP].psrc)
            response_mac = packet[scapy.ARP].hwsrc

            if real_mac != response_mac:
                print("[+] You are under attack!")
        
        except IndexError:
            pass

sniff("eth0")
 