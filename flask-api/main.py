from flask import Flask, send_from_directory
import os

app = Flask(__name__)

@app.route("/")
def hello_world():
    return "Hello World!"

@app.route("/file")
def download_file():
    print("Helloo" + app.root_path)
    return send_from_directory(app.root_path + "/", "test.exe", as_attachment=True)

app.run(host="0.0.0.0", port=5000, debug=True)
