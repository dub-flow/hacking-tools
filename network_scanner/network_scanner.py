#!/usr/bin/env python

# example usage: "python3 network_scanner.py --target 10.0.2.1/24"

import scapy.all as scapy
import argparse as ap

def get_arguments():
    parser = ap.ArgumentParser()
    parser.add_argument("-t", "--target", dest="target", help="Target IP / IP range.")
    options = parser.parse_args()

    if not options.target:
        parser.error("[-] Please specify a target, use --help for more info.")

    return options

# network scanner using scapy (Boring!) # usage: scapy_scan("10.0.2.1/24") 
def scapy_scan(ip):
    scapy.arping(ip)

# writing our own network scanner
def scan(ip):
    print (f"Scanning network for IP: {ip}... \n")

    # create an ARP object and set destination IP
    arp_request = scapy.ARP(pdst=ip)
    # create an ethernet frame with the broadcast MAC address
    broadcast = scapy.Ether(dst="ff:ff:ff:ff:ff:ff")
    # combine both objects
    arp_request_broadcast = broadcast / arp_request

    # print(arp_request_broadcast.summary())
    # arp_request.show()
    # broadcast.show()
    # arp_request_broadcast.show()

    # send provided packet and receive the responses -> We don't need to provide a destination. The packet will 
    # go to the right direction because we added an Ether layer to it
    answered_packets = scapy.srp(arp_request_broadcast, timeout=1, verbose=False)[0]
    # print(answered_packets.summary())

    clients_list = list()
    for _, answer in answered_packets:
        client_dict = {"ip": answer.psrc, "mac": answer.hwsrc}
        clients_list.append(client_dict)

    return clients_list

def print_result(clients_list):
    print("IP \t\t\t MAC Address \n----------------------------------------------------")
    for client in clients_list:
        print(client["ip"] + "\t\t" + client["mac"])

options = get_arguments()
scan_result = scan(options.target)
print_result(scan_result)
