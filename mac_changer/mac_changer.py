#!/usr/bin/env python

# example usage: "python3 mac_changer.py --interface eth0 --mac 00:11:22:44:55:33"

import subprocess
import optparse as op
import re

def get_arguments():
    parser = op.OptionParser()
    parser.add_option("-i", "--interface", dest="interface", help="Interface to change its MAC address")
    parser.add_option("-m", "--mac", dest="new_mac", help="The new MAC address to assign to the interface")
    (options, arguments) = parser.parse_args()

    if not options.interface:
        parser.error("[-] Please specify an interface, use --help for more info.")

    if not options.new_mac:
        parser.error("[-] Please specify a new mac, use --help for more info.")

    return options, arguments

def change_mac_of_interface(interface, new_mac):
    print(f"[+] Changing MAC address for '{interface}' to: {new_mac}")

    subprocess.call(["sudo", "ifconfig", interface, "down"])
    subprocess.call(["sudo", "ifconfig", interface, "hw", "ether", new_mac])
    subprocess.call(["sudo", "ifconfig", interface, "up"])
    # subprocess.call("ifconfig", shell=True)

def get_current_mac(interface):
    ifconfig_result = subprocess.check_output(["ifconfig", interface])
    # print(ifconfig_result)

    # regex to search for the MAC address in an ifconfig result
    current_mac_search_result = re.search(r"\w\w:\w\w:\w\w:\w\w:\w\w:\w\w", str(ifconfig_result))
    
    if current_mac_search_result:
        current_mac = current_mac_search_result.group(0)
        print(f"[+] Current MAC of interface '{interface}' is: {current_mac}")
        return current_mac
    else:
        print(f"[-] Could not read current MAC address of interface '{interface}'")
        return

# get the user input
options, _ = get_arguments()
interface = options.interface
new_mac = options.new_mac

current_mac = get_current_mac(interface)
change_mac_of_interface(interface, new_mac)
current_mac = get_current_mac(interface)

# check if the mac changed as expected
if current_mac == new_mac:
    print(f"[+] MAC address was successfully changed to: {current_mac}")
else:
    print("[-] Changing MAC address failed")
